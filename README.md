# Monadic

The aims of this small library is to provide a JS-friendly set of tools for creating and handling monads in an object-oriented way.  This provides a set of convenient factory methods for creating monads and the monads themselves are plain JS objects with appropriate methods like `chain`, `join`, `map`, etc.

The library is written in ES6 syntax using ES modules.  While it could have been written in Typescript, plain JS was chosen to avoid any need for compilation and be importable as is in both Node and Deno.  It does however come fully typed using a mix of Typescript declaration files and JS Doc with type-checking a core part of its test suite.  Examples will therefore be written in Typescript.

## Usage

```ts
import { Maybe } from '@heliopolis/monadic'

Maybe.of(null)
     .map((x: number) => x * 2)
// => Maybe<null>
```

## Installation

```sh
$ npm install --save @heliopolis/monadic
$ pnpm add --save-dev @heliopolis/monadic
$ yarn add @heliopolis/monadic
```

If you wish to use this package without build tools, import it directly in the browser using an ESM-friendly CDN.

```html
<script type="module">
  import { Either } from 'https://cdn.skypack.dev/@heliopolis/monadic'
</script>
```

## Roadmap

| Package features    | Supported |
|:--------------------|:---------:|
| 100% test coverage  |  &#x2713; |
| 100% type coverage  |  &#x2713; |
| Automated releases  | |
| Monad documentation | |


| Monads and Methods  | Supported |  
|:--------------------|:---------:|
| Either `.create`    | |
| Either `.of`        | |
| Either `#chain`     | |
| Either `#fold`      | |
| Either `#getOrElse` | |
| Either `#join`      | |
| Either `#map`       | |
| Identity `.create`  |  &#x2713; |
| Identity `#chain`   |  &#x2713; |
| Identity `#join`    |  &#x2713; |
| Identity `#map`     |  &#x2713; |
| Identity `#value`   |  &#x2713; |
| Maybe `.create`     | |
| Maybe `.of`         | |
| Maybe `#chain`      | |
| Maybe `#fold`       | |
| Maybe `#getOrElse`  | |
| Maybe `#join`       | |
| Maybe `#map`        | |