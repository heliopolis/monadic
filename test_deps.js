import chai from 'chai'
export const { expect } = chai

export { beforeEach, describe, it } from 'mocha'

import sinon from 'sinon'
export const { fake } = sinon

import sinonChai from 'sinon-chai'
chai.use(sinonChai)
