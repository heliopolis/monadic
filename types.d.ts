declare namespace Monadic {
  interface Identity<T> {
    chain(f: Unary<T, Identity<U>>): Identity<U>
    join(): T
    map<U>(f: Unary<T, U>): Identity<U>
    readonly value: T
  }
}

interface Unary<T = unknown, U = unknown> {
  (value: T): U
}
