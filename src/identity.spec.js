import { describe, expect, fake, it } from '../test_deps.js'
import { create, of } from './identity.js'

describe('Identity', () => {
  describe('.create :: a -> Identity a', () => {
    it('returns an Identity containing the provided value of type `a`', () => {
      const value = 42

      const result = create(value)

      expect(result.value).to.equal(value)
    })

    it('returns an Identity monad that is frozen', () => {
      const result = create(undefined)

      expect(result).to.be.frozen
    })
  })

  describe('.of :: a -> Identity a', () => {
    it('returns an Identity containing the provided value of type `a`', () => {
      const value = Symbol()

      const result = of(value)

      expect(result.value).to.equal(value)
    })
  })

  describe('Identity a', () => {
    /**
     * @param {number} x
     * @returns {number}
     */
    function double(x) {
      return x * 2
    }

    describe('#chain :: (a -> Identity b) -> Identity b', () => {
      it('applies the given function to the contained value', () => {
        const f = fake(double)
        const monad = of(42)

        monad.chain(f)

        expect(f).to.have.been.calledOnceWithExactly(42)
      })

      it('returns the result of applying the given function to the contained value', () => {
        const monad = of(42)

        const result = monad.chain(create)

        expect(result.value).to.equal(42)
      })
    })

    describe('#join :: () -> a', () => {
      it('returns the contained value', () => {
        const value = Symbol('example value')
        const monad = of(value)

        const result = monad.join()

        expect(result).to.equal(value)
      })
    })

    describe('#map :: (a -> b) -> Identity b', () => {
      const value = 42

      it('applies the given function to the contained value', () => {
        const f = fake(double)
        const monad = of(value)

        monad.map(f)

        expect(f).to.have.been.calledOnceWithExactly(42)
      })

      it('returns the result of applying the given function to the contained value wrapped in an Identity', () => {
        const monad = of(value)

        const result = monad.map(double)

        // Identity.of(a).map(f) === Identity.of(f(a))
        expect(result.value).to.equal(double(value))
      })
    })

    describe('#value :: a', () => {
      it('is the contained value', () => {
        const value = Symbol()

        const result = of(value).value

        expect(result).to.equal(value)
      })
    })
  })
})
