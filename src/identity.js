/**
 * @template T
 * @param {T} value
 * @return {Monadic.Identity<T>}
 */
export function create(value) {
  return Object.freeze({
    chain(f) {
      return f(value)
    },
    join() {
      return value
    },
    map(f) {
      return create(f(value))
    },
    value
  })
}

export const of = create
