import { describe, expect, it } from '../test_deps.js'

import * as Monadic from './index.js'

describe('Monadic', () => {
  it('exports an Identity namespace', () => {
    expect(Monadic).to.have.ownProperty('Identity')
  })
})
